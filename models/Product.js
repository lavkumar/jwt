const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
{
    name: { type: String},
    desc: { type: String},
    image_url: { type: String},
    
},{
   timestamps:true
});
module.exports=mongoose.model("product",ProductSchema)