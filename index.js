const express = require("express");
const bodyParser= require('body-parser');
const app =express();
const mongoose = require ("mongoose");
const dotenv = require("dotenv");
dotenv.config();
const userRouter = require("./routes/user")
const authRouter = require("./routes/auth")
const Router = require("./routes/product")

 // Connecting to the database
 mongoose
 .connect(process.env.url, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
  
 })
 .then(() => {
   console.log("Successfully connected to database");
 })
 .catch((error) => {
   console.log("database connection failed. exiting now...");
   console.error(error);
   process.exit(1);
 });

 
//body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use("/api/user",userRouter);
app.use("/api/auth",authRouter);
app.use("/api/products",Router);


app.listen(process.env.PORT||5000,()=>{
    console.log("backend server is running")
})