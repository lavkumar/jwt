const { verify } = require("jsonwebtoken");
const {  verifyTokenAndAdmin } = require("./verifyToken");
const Product = require("../models/Product");

const router = require("express").Router();
//crate products
router.post("/",verifyTokenAndAdmin, async(req,res)=>{
    const newproduct = new Product(req.body)
    try{
        const savedproduct =await newproduct.save();
        res.status(200).json(savedproduct)
    }catch(err){
        res.status(500).json(err)
    }
});
// //Update
router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        const updatedProduct = await Product.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );
        res.status(200).json(updatedProduct)
    } catch (err) {
        res.status(500).json(err);
    }

});
// // delete
router.delete("/delete/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        await Product.findByIdAndDelete(req.params.id)
        res.status(200).json("product are deleted...")
    } catch (err) {
        res.status(500).json(err)
    }
});


// // get product

router.get("/find/:id",  async (req, res) => {
    try {
        const product = await Product.findById(req.params.id)
        
        res.status(200).json(product);
    } catch (err) {
        res.status(500).json(err)
    }
});
// get all products

router.get("/all", async (req, res) => {
    try {
        const products = await Product.find()

        res.status(200).json(products);
    } catch (err) {
        res.status(500).json(err)
    }
});
module.exports = router
